import pandas as pd
import numpy as np
from datetime import datetime


def merge_csv(customers_files, loans_files, visits_files):
    """
    Use:
        imports files into three different datasets,
        cleans datasets
        merges datasets into one
    Arguments:
        customers_files: list of one customers file
        loans_files: list of loans files
        visits_files: list of one visits file
    Returns:
        final Pandas Dataframe
    """
    # customers
    customers = pd.read_json(customers_files[0], lines=True)
    customers = customers.rename(columns={"id": "user_id"})
    customers['zip_code'] = customers['zip_code'].str.replace(' ', '')
    customers['zip_code'] = customers['zip_code'].apply(lambda x: x[3:] if len(x)>5 else x)
    customers['zip_code'] = customers['zip_code'].apply(lambda x: zip_code_norm(x) if len(x) == 5 else x)


    # visits
    visits = pd.read_csv(visits_files[0])
    visits = visits.iloc[:, 1:].rename(columns={"id": "visit_id"}) # we delete the first column, useless
    visits = visits.rename(columns={"timestamp": "visit_timestamp"})
    visits['visit_datetime'] = visits.visit_timestamp.apply(lambda x: datetime.fromtimestamp(x))
    visits['visit_date'] = visits['visit_datetime'].apply(lambda x: x.date())
    visits['visit_id'] = visits['visit_id'].astype(str) # to fit with the webvisit_id feature in the loans dataframe

    # loans
    loans_import = list()
    for l in loans_files:
        loans_import.append(pd.read_csv(l))
    loans = pd.concat(loans_import, ignore_index=True)
    loans = loans.rename(columns={"id": "loan_id"})
    loans = loans.iloc[:, 1:]
    loans["webvisit_id"] = np.where(loans["webvisit_id"].isna(), '-1', loans["webvisit_id"])
    loans['webvisit_id'] = np.where(loans["webvisit_id"] =='-1', '-1', loans["webvisit_id"].str[1:-2])
    loans = loans.rename(columns={"timestamp": "loan_timestamp"})
    loans['loan_datetime'] = loans['loan_timestamp'].apply(lambda x: datetime.fromtimestamp(x))
    loans['loan_date'] = loans['loan_datetime'].apply(lambda x: x.date())
    loans['loan_purpose'] = loans['loan_purpose'].replace({'buying a pet': 'Buying a pet', 'home purchase': 'Home purchase'})

    # first merge, between loans and customers
    merge1 = loans.merge(customers, on=['user_id'])

    # second merge, with visits df
    merge2 = merge1.merge(visits, left_on=['webvisit_id'], right_on=['visit_id'])

    # sort df by loan datetime then by visit datetime
    merge2 = merge2.sort_values(by=['loan_datetime', 'visit_datetime'])

    return merge2


def zip_code_norm(z):
    """
    adapted two our situation
    correct the format of the zip code
    Arguments:
        z: zip code (XXXXX)
    Returns:
        z2: corrected zip code (XXX XX)
    """
    lchar = list(z)
    z2 = ''.join(lchar[:3] + [' '] + lchar[3:])
    return z2